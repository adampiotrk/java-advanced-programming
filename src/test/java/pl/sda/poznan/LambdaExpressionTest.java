package pl.sda.poznan;

import org.junit.Before;
import org.junit.Test;
import pl.sda.poznan.shop.model.Product;

import java.util.*;
import java.util.stream.Collectors;

import static org.junit.Assert.*;

public class LambdaExpressionTest {

    private List<Product> products;

    @Before
    public void init() {
        Product smartphone = new Product("Nokia", "Phone", 500D);
        Product laptop = new Product("Dell", "Laptop", 4500D);
        Product console = new Product("PlayStation", "4th generation", 2000D);

        this.products = Arrays.asList(smartphone, laptop, console);
    }

    // playground for testing lambda
    @Test
    public void test() {
        //find products with prices > 1000
        List<Product> result = products
                .stream()
                .filter(product -> product.getPrice() > 1000)
                .collect(Collectors.toList());

        assertEquals(2, result.size());
        assertEquals(3, products.size());
    }

    @Test
    public void should_find_where_starts_with_given_letter() {
        //find products with price > 1000 and first letter "P"
        List<Product> result = products
                .stream()
                .filter(product -> product.getPrice() > 1000)
                .filter(product -> product.getName().startsWith("P"))
                .collect(Collectors.toList());

        assertEquals(1, result.size());
        assertEquals(3, products.size());
    }

    @Test
    public void should_sort_alphabetical() {
        List<Product> result = products
                .stream()
                .sorted(Comparator.comparing(p -> p.getName()))
                .collect(Collectors.toList());

        assertEquals("Dell", result.get(0).getName());
        assertEquals("Nokia", result.get(1).getName());
        assertEquals("PlayStation", result.get(2).getName());
    }

    @Test
    public void should_sort_by_price() {
        this.products.sort(Comparator.comparing(Product::getPrice));
        products.get(0).getPrice();

    }

    @Test
    public void should_find_first_with_price_less_than_2000() {
        Optional<Product> first = products
                .stream()
                .filter(p -> p.getPrice() < 2000)
                .findFirst();

        // Product p = first.orElseThrow(() -> new NoSuchElementException()); - robi to samo co funkcja nizej, pobiera produkt, a jesli go nie ma to rzuca wyjatek
        Product p = first.orElseThrow(NoSuchElementException::new);

        assertEquals(products.get(0), p);
    }


    @Test
    public void should_create_new_list_only_with_names() {
        List<String> result = this.products
                .stream()
                .map(product -> product.getName())
                .collect(Collectors.toList());

        assertEquals(products.get(0).getName(), result.get(0));
        assertEquals(products.get(1).getName(), result.get(1));
        assertEquals(products.get(2).getName(), result.get(2));

    }

}