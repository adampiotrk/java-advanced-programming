package pl.sda.poznan.peselValidation;

import org.junit.Test;

import static org.junit.Assert.*;

public class PeselValidationTest {
    @Test
    public void should_validate_pesel_true() {
        assertEquals(true, PeselValidation.validate(92121608672L));
        assertEquals(false, PeselValidation.validate(44051401358L));
    }

    @Test(expected = IllegalArgumentException.class)
    public void should_throw_exception_when_no_11_digits() {
        PeselValidation.validate(9554);
    }

}