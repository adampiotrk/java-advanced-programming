package pl.sda.poznan.math;

import org.junit.Test;

import static org.junit.Assert.*;

public class CalculatorTest {

    @Test
    public void should_return_zero_when_empty_string() {
        int sum = Calculator.sum("");
        assertEquals(0, sum);
    }

    @Test
    public void should_return_number_when_one_number_is_given() {
        int number = Calculator.sum("2");
        assertEquals(2, number);
    }

    @Test
    public void should_sum_two_numbers() {
        int result = Calculator.sum("2,3");
        assertEquals(5, result);
    }

    @Test
    public void should_sum_multiple_numbers() {
        int result = Calculator.sum("2,3,4,5");
        assertEquals(14, result);
    }

    @Test
    public void should_sum_numbers_in_multiple_lines() {
        String string = "2,3\n4,5";
        System.out.println(string);
//        int result = Calculator.sum("2,3\n4,5");
//        assertEquals(14, result);
    }

    @Test(expected = IllegalArgumentException.class)
    public void should_throw_exception_when_factorial_less_than_zero() {
        Calculator.factorial(-2);
    }

    @Test
    public void should_factorial_numbers_bigger_than_zero() {
        assertEquals(1, Calculator.factorial(0));
        assertEquals(1, Calculator.factorial(1));
        assertEquals(2, Calculator.factorial(2));
        assertEquals(6, Calculator.factorial(3));
        assertEquals(24, Calculator.factorial(4));
        assertEquals(120, Calculator.factorial(5));
    }


}