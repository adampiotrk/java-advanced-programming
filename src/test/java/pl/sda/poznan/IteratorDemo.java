package pl.sda.poznan;

import org.junit.Test;

import java.util.Arrays;
import java.util.List;

public class IteratorDemo {

    @Test
    public void test() {
        List<String> stringList = Arrays.asList("Adam", "Piotr", "Damian");

        // dzieki implementacji interfejsu iterable mozna iterowac po elemencie, tak dziala petla for each
        while (stringList.iterator().hasNext()) {
            String element = stringList.iterator().next();
            System.out.println(element);
        }
    }
}
