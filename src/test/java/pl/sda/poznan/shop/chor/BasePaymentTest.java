package pl.sda.poznan.shop.chor;

import org.junit.Test;

import static org.junit.Assert.*;

public class BasePaymentTest {

    @Test
    public void pay() {
        Account account = new Account();
        account.setBalance(1000D);
        Payment paypass = new PaypassPayment(account);
        Payment pinPayment = new PinPayment(account);

        paypass.setNextHandler(pinPayment);

        // wywolujemy na bazowym, jak nie bedzie spelniac warunku to przejdzie do metody do platnosci z pinem
        boolean isSuccess = paypass.handle(200D);
        boolean isFalse = paypass.handle(2000D);

        assertEquals(true, isSuccess);
        assertEquals(false, isFalse);
    }

}