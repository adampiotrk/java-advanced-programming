package pl.sda.poznan.greet;

import org.junit.Test;

import static org.junit.Assert.*;

public class GreetingsTest {

    @Test
    public void should_greet_with_one_name() {
        String welcome = Greetings.greet("Jan");
        assertEquals("Hello, Jan", welcome);
    }

    @Test
    public void should_greet_with_null() {
        String welcome = Greetings.greet(null);
        assertEquals("Hello, my friend", welcome);
    }

    @Test
    public void should_greet_with_capital_case() {
        String welcome = Greetings.greet("JAN");
        assertEquals("HELLO, JAN", welcome);
    }

    @Test
    public void should_greet_with_two_names() {
        String welcome = Greetings.greet("Jan", "Ala");
        assertEquals("Hello, Jan and Ala", welcome);
    }

    @Test
    public void should_greet_with_multiple_names() {
        String welcome = Greetings.greet("Jan", "Ala", "Paulina");
        assertEquals("Hello, Jan, Ala and Paulina", welcome);
    }

    @Test
    public void should_greet_with_multiple_names_and_capital_name() {
        String welcome = Greetings.greet("JAN", "Ala", "Paulina");
        assertEquals("Hello, Ala and Paulina. HELLO, JAN", welcome);
    }

}