package pl.sda.poznan.primeNumbers;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;
import org.junit.runners.Parameterized.Parameters;

import java.util.Arrays;
import java.util.Collection;

import static org.junit.Assert.*;

@RunWith(Parameterized.class)
public class PrimeNumbersTest {

    private int number;
    private boolean isPrime;

    public PrimeNumbersTest(int number, boolean isPrime) {
        this.number = number;
        this.isPrime = isPrime;
    }

    @Parameters
    public static Collection<Object[]> data() {
        return Arrays.asList(new Object[][]{
                {2, true},
                {3, true},
                {4, false},
                {5, true},
                {6, false},
                {7, true},
                {8, false},
                {9, false},
        });
    }

    @Test
    public void should_return_true_when_prime_number() {
        assertEquals(isPrime, PrimeNumbers.primeNumber(number));


    }


}