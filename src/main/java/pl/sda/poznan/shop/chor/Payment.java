package pl.sda.poznan.shop.chor;

public interface Payment {

    boolean handle(Double Amount);

    void setNextHandler(Payment nextHandler);

}
