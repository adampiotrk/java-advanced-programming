package pl.sda.poznan.shop.chor;

public abstract class BasePayment implements Payment {

    protected Account account;

    protected Payment nextPayment;

    public BasePayment(Account account) {
        this.account = account;
    }

    @Override
    public boolean handle(Double amount) {
        if (canHandle(amount)) {
            //obsluga platnoosci
            return pay(amount);
        } else if (nextPayment != null) {
            return nextPayment.handle(amount);
        }
        return false;
    }

    @Override
    public void setNextHandler(Payment nextHandler) {
        this.nextPayment = nextHandler;
    }

    protected abstract boolean canHandle(Double amount);

    // metoda platnosci
    protected abstract boolean pay(Double amount);
}
