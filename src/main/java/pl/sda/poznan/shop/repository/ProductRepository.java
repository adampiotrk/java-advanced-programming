package pl.sda.poznan.shop.repository;

import pl.sda.poznan.shop.exception.ProductNotFoundException;
import pl.sda.poznan.shop.model.Product;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

public class ProductRepository {
    //tworzenie wzorca Singleton - chcemy ograniczyc mozliwosci tworzenia obiektow do jednego

    private static ProductRepository instance = new ProductRepository();

    private ProductRepository() {
    }

    public static ProductRepository getInstance() {
        return instance;
    }

    private static Long productId = 1L;

    private List<Product> products = new ArrayList<>();

    public Product add(Product product) {
        //set productId of the product
        product.setId(productId++);
        this.products.add(product);
        return product;
    }

    public void add(List<Product> products) {
        // wyrazenie lamba
//        products.forEach(p -> this.add(p));
        //method reference
        products.forEach(this::add);
    }

    public List<Product> getAll() {
        return this.products;
    }

    public Product getById(Long id) {
        return this.products
                .stream()
                .filter(product -> product.getId().equals(id))
                .findFirst()
                .orElseThrow(() -> new ProductNotFoundException("Nie ma produktu o takim identyfikatorze"));
    }

    public Product getByName(String name) {
        return this.products
                .stream()
                .filter(pr -> pr.getName().equals(name))
                .findFirst()
                .orElseThrow(() -> new ProductNotFoundException("Nie ma produktu o podanej nazwie"));
    }

    public boolean remove(Long id) {
        return this.products.removeIf(p -> p.getId().equals(id));

    }

    public boolean remove(Product product) {
        return this.products.remove(product);
    }

    public int count() {
        return this.products.size();
    }
}
