package pl.sda.poznan.shop;

import pl.sda.poznan.shop.model.CartItem;
import pl.sda.poznan.shop.model.Product;
import pl.sda.poznan.shop.model.ShopCart;
import pl.sda.poznan.shop.repository.ProductRepository;

import java.util.Arrays;
import java.util.Scanner;

public class Program {

    public static void main(String[] args) {

        initializeApp();
        ShopCart cart = new ShopCart();
        Scanner scanner = new Scanner(System.in);
        boolean exit = false;

        while (!exit) {
            printMenu();
            int choice = scanner.nextInt();
            switch (choice) {
                case 1:
                    //showing list of products
                    System.out.println("Lista produktow:");
                    ProductRepository.getInstance()
                            .getAll()
                            .forEach(System.out::println);
//                            .forEach(p -> System.out.println(p.toString())); - inny sposob na ten sam zapis

                    break;
                case 2:
                    System.out.println("Podaj id produktu");
                    Long productId = scanner.nextLong();
                    Product product = ProductRepository
                            .getInstance()
                            .getById(productId);
                    System.out.println("Wybrales: " + product);

                    // logika dodawania do koszyka
                    System.out.println("Podaj ilosc: ");
                    int quantity = scanner.nextInt();
                    CartItem cartItem = new CartItem.CartItemBuilder()
                            .name(product.getName())
                            .description(product.getDescription())
                            .price(product.getPrice())
                            .quantity(quantity)
                            .build();

                    cart.add(cartItem);
                    System.out.println("Dodano element do koszyka. Aktualna wartosc zakupow to: " + cart.getSum());
                    break;
                case 8:

                    break;
                case 9:
                    //log user in
                    System.out.println("Logowanie:");
                    System.out.println("Podaj login:");
                    System.out.println("Podaj hasło:");
                    break;
                case 10:
                    //create new account
                    System.out.println("Rejestracja:");
                    System.out.println("Wprowadz dane:");
                    System.out.println("Imie:");
                    System.out.println("Nazwisko:");
                    System.out.println("Email:");
                    System.out.println("Haslo:");
                    System.out.println("Powtorz haslo:");
                    break;
                case 0:
                    //exit
                    System.out.println("Wychodze z programu...");
                    exit = true;
                    break;

            }
        }
    }

    private static void printMenu() {
        System.out.println(" ");
        System.out.println("Menu:");
        System.out.println("1. Wyświetl wszystkie produkty");
        System.out.println("2. Znajdz produkt po identyfikatorze");

        System.out.println("8. Zapisz produkty do pliku");
        System.out.println("9. Logowanie");
        System.out.println("10. Rejestracja");
        System.out.println("0. Wyjście");

    }

    private static void initializeApp() {
        seedProducts();
    }

    private static void seedProducts() {
        Product smartphone = new Product("Nokia", "Phone", 500D);
        Product laptop = new Product("Dell", "Laptop", 4500D);
        Product console = new Product("PlayStation", "Console", 2000D);

        ProductRepository
                .getInstance()
                .add(Arrays.asList(smartphone, laptop, console));
    }
}

