package pl.sda.poznan.peselValidation;

public class PeselValidation {
    public static boolean validate(long pesel) {
        char[] digits = Long.toString(pesel).toCharArray();
        if (digits.length != 11) {
            throw new IllegalArgumentException("Pesel number should have 11 numbers");
        }

        int[] numbers = new int[digits.length];
        for (int i = 0; i < numbers.length; i++) {
            numbers[i] = (digits[i] - 48);
        }

        int checker = 9 * numbers[0] + 7 * numbers[1] + 3 * numbers[2] + numbers[3] + 9 * numbers[4] + 7 * numbers[5] + 3 * numbers[6] + numbers[7] + 9 * numbers[8] + 7 * numbers[9];
        return (checker % 10 == numbers[10]);
    }
}
