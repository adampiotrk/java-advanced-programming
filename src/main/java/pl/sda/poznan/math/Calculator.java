package pl.sda.poznan.math;

public class Calculator {
    public static int sum(String str) {
        if (str.isEmpty()) {
            return 0;
//        } else if (str.length() == 1) {
//            return parseInteger(str);
        } else {
            String[] split = str.split(",");
            Integer sum = 0;
            for (int i = 0; i < split.length; i++) {
                sum = sum + parseInteger(split[i]);
            }
            return sum;
        }
    }

    private static int parseInteger(String s) {
        return Integer.parseInt(s);
    }

    public static int factorial(int i) {
        if (i < 0) {
            throw new IllegalArgumentException("Factorial must be greater than 0");
        } else if (i == 0) {
            return 1;
        } else {
            return (i * factorial(i - 1));
        }
    }
}
