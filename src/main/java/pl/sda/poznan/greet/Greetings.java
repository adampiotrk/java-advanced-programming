package pl.sda.poznan.greet;

public class Greetings {
    public static String greet(String... names) {
        if (names == null) {
            return ("Hello, my friend");
        }
        if (names.length == 1) {
            String name = names[0];
            if (name.equals(name.toUpperCase())) {
                return ("HELLO, " + name);
            } else {
                return ("Hello, " + name);
            }
        } else {
            StringBuilder lowerCase = new StringBuilder();
            StringBuilder upperCase = new StringBuilder();
            lowerCase.append("Hello");
            upperCase.append(". HELLO");
            for (int i = 0; i < names.length - 1; i++) {
                if (names[i].equals(names[i].toUpperCase())) {
                    upperCase.append(", ").append(names[i]);
                } else {
                    lowerCase.append(", ").append(names[i]);
                }
            }
            lowerCase.append(" and ").append(names[names.length - 1]);
            lowerCase.append(upperCase);
            return lowerCase.toString();

        }
    }
}

